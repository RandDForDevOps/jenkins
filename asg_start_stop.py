import asgops as so
import json
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--region', '-r', help='AWS region', required=False, default='us-east-1')
parser.add_argument('--env', '-e', help='environment', required=True)
parser.add_argument('--asg_name', '-n', help='autoscaling group name', required=True)
parser.add_argument('--action', '-a', help='action to start or stop', required=True)
parser.add_argument('--logfile', '-l', help='Write logs to file', required=False, default='_startstoplogs')
args = parser.parse_args()
#logger= so.get_logger(args.logfile)
logger= args.logfile
if args.env == 'development':
  region = args.region
  asg = args.asg_name
  timeout = 900
  if args.action== 'start':
    so.asg_operations(region, asg, args.action, timeout, logger)
  elif args.action == 'stop':
    so.asg_operations(region, asg, args.action, timeout, logger)
elif args.env == 'staging':
  region = args.region
  asg = args.asg_name
  timeout = 900
  if args.action == 'start':
    so.asg_operations(region, asg, args.action, timeout, logger)
  elif args.action == 'stop':
    so.asg_operations(region, asg, args.action, timeout, logger)
else:
  raise ValueError('Please enter valid environment name')