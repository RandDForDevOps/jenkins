#!/usr/bin/env python
#
# Filename: stackops
# Author: Shalmali sahasrabudhe
# Copyright OpsFuse
# Created: Wed Mar 13 19:34:47 2014 (+0530)
# Last-Updated: Tue Jun 21 12:46:14 2016 (-0700)
# Description: Contains common functionalities used during various autoscaling group operations
#

import os
import sys
import time
import json
import logging
import boto3
import subprocess as sp

def get_formatter():
  return logging.Formatter(fmt='%(asctime)s : %(levelname)-5s : %(message)s', datefmt='%Y-%m-%d %H:%M:%S')

# Get logger

def get_logger( logfile ):
    global logger
    logging.basicConfig(filename=logfile,
                    format='%(asctime)s %(message)s',
                    filemode='w')
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    return logger

def list_instances(instances):
    return ", ".join([str(x).encode('utf-8') for x in instances.values()])

def startInstances(ec2, instances, timeout, logger):
    waited = 0
    s_instances = []
    i_total = len(instances)
    print('total: ', i_total)
    sleeptime = 10
    if not instances:
        logger.info("Instance list provided is empty. Return successfully.")
        return True
    try:
        print(instances.keys())
        s_instances = ec2.start_instances(InstanceIds=instances.keys())
        s_instances = [  i['InstanceId'] for i in s_instances['StoppingInstances'] ]
    except Exception, e:
        logger.error("Failed to start instances: [%s]. Error: %s", list_instances(instances), str(e))
        return False

    if len(s_instances) == i_total:
        logger.info("Initiated start of instances: [%s]", list_instances(instances))
        print("Initiated start of instances: [%s]", s_instances)
    else:
        # TODO: List actual instances that failed to start
        logger.error("Failed to initiate start for one or more instances: [%s]", list_instances(instances))
        return False

    # Wait until instances are running
    logger.info("Waiting for instance(s) to be started ... (Max wait %s sec)", timeout)
    while len(s_instances) > 0 and waited < timeout:
        try:
            i_status = c.get_only_instances(s_instances, filters={ 'instance-state-name' : 'running' })
        except Exception, e:
            logger.error("Failed to get instances status. Error:\n%s", str(e))
            return False

        # Get id for all stopped instances and remove them from the list of running instances
        running = [ i.id for i in i_status ]
        s_instances = [ i for i in s_instances if i not in running ]

        if(running):
            logger.debug("Instances now running: %s", ", ".join(instances[i] for i in running))

        if len(s_instances) > 0:
            logger.info("Waiting for instance(s) [%s] to be running (%s sec elapsed).",
                        ", ".join(instances[i] for i in s_instances), waited)
            waited += sleeptime # seconds
            time.sleep(sleeptime)

    if len(s_instances) > 0:
        for i in s_instances:
            logger.error("Failed to start Instance:%s Id:%s in timeout period: %s sec",
                         str(instances[i]).encode('utf-8'), i, str(timeout))
        return False
    else:
        logger.info("Successfully started instances: [%s]", list_instances(instances))

    return True


def stopInstances(ec2, instances, timeout, logger):
    waited = 0
    r_instances = []
    i_total = len(instances)
    sleeptime = 10
    if not instances:
        logger.info("Instance list provided is empty. Return successfully.")
        return True
    try:
        print(instances.keys())
        r_instances = ec2.stop_instances(InstanceIds=instances.keys())
        print(r_instances)
        r_instances = [ i['InstanceId'] for i in r_instances['StoppingInstances'] ]
        print(r_instances)
    except Exception, e:
        logger.error("Failed to stop instances: [%s]. Error: %s", list_instances(instances), str(e))
        return False
    if len(r_instances) == i_total:
        logger.info("Initiated stop of instances: [%s]", r_instances)
        print("Initiated stop of instances: [%s]", r_instances)
    else:
        # TODO: List actual instances that failed to start
        logger.error("Failed to initiate stop for one or more instances: [%s]", list_instances(instances))
        return False

    # Wait until instances are down
    #logger.info("Waiting for instance(s) to be stopped ... (Max wait %s sec)", timeout)
    while len(r_instances) > 0 and waited < timeout:
        i_status = []
        try:
            i_status = ec2.describe_instance_status(r_instances, filters={ 'instance-state-name' : 'stopped' })
        except Exception, e:
            logger.error("Failed to get instances status. Error:\n%s", str(e))
            return False

        # Get id for all stopped instances and remove them from the list of running instances
        stopped = [ i.id for i in i_status ]
        r_instances = [ i for i in r_instances if i not in stopped ]

        if(stopped):
            logger.debug("Instances stopped: %s", ", ".join(instances[i] for i in stopped))

        if len(r_instances) > 0:
            logger.info("Waiting for instance(s) [%s] to be stopped (%s sec elapsed).",
                        ", ".join(instances[i] for i in r_instances), waited)
            waited += sleeptime # seconds
            time.sleep(sleeptime)

    if len(r_instances) > 0:
        for i in r_instances:
            logger.error("Failed to stop Instance:%s Id:%s in timeout period:%s sec",
                         str(instances[i]).encode('utf-8'), i, str(timeout))
        return False
    else:
        logger.info("Successfully stopped all instances: [%s]", list_instances(instances))

    return True

def asg_operations(region, asg_name, action, timeout, logfile):
    logger=get_logger(logfile)
    ec2 = boto3.client('ec2',region_name=region)
    autoscale = boto3.client('autoscaling',region_name=region)
    elb = boto3.client('elb',region_name=region)
    try:
        asgs = []
        elbs = []
        instances = {}
        natinstance = {}
        chefinstance = {}
        oracleinstance = {}
        asg = autoscale.describe_auto_scaling_groups(AutoScalingGroupNames=[asg_name])
        #print('asg description: ',asg)
        #print(asg['AutoScalingGroups'][0]['instances'])
        for i in asg['AutoScalingGroups']:
            #print('inside AutoScalingGroups')
            for k in i['Instances']:
                #print('inside instances')
                #instances.append(asg_name + ':' + k['InstanceId'])
                instances[k['InstanceId']] = asg_name + ':' + k['InstanceId']
                #print(k['InstanceId'])
                asgs.append({
                   "id": asg_name,
                   "name": asg_name,
                   "object": autoscale,
                   "instances": [ k['InstanceId'] ]
                })
        print(asgs)
        elb = autoscale.describe_load_balancers(AutoScalingGroupName=asg_name)
        print('elb description: ',elb)
        if not elb['LoadBalancers']:
            pass
        else:
            for e in elb['LoadBalancers']:
                lbs = elb.describe_load_balancers(LoadBalancerNames=[e.LoadBalancerName])

                elbs.append({
                    "object": elb,
                    "name": e.LoadBalancerName,
                    "instances":[i.instance_id for i in lbs['LoadBalancerDescriptions'].instances]
                })

    except Exception, e:
        if 'does not exist' in e.message:
            logger.debug('ASG "%s" does not exist', asg_name)
        else:
            raise Exception('Could not describe ASG. Exception : %s', e.message)

    if "stop" in action:
        for asg in asgs:
            print(asg['name'])
            logger.info("Suspending processes on ASG")
            autoscale.suspend_processes(AutoScalingGroupName=asg['name'], ScalingProcesses=['HealthCheck', 'AlarmNotification',
                                                               'AZRebalance', 'ScheduledActions', 'Launch',
                                                               'ReplaceUnhealthy', 'AddToLoadBalancer'] )

            #instances.append(asg['instances'])
        print(instances)
        # First stop rest of the instances then nat followed by chef
        if not stopInstances(ec2, instances, timeout, logger):
            logger.error("Failed to stop instances")
            exit(-1)

    if action == "start":
        # First start chef, then nat, finally the rest of the instances
        if not startInstances(ec2, instances, timeout, logger):
            logger.error("Failed to start instances")
            exit(-1)

        for e in elbs:
            e['object'].deregister_instances_from_load_balancer(e['name'],e['instances'])
            e['object'].register_instances_with_load_balancer(e['instances'],e['name'])
        for e in elbs:
            _timeout = 900
            OutOfService = True
            while(_timeout > 0 and OutOfService):
                instances = elb_conn.describe_instance_health(e['name'])
                OutOfService = False
                for instance in instances:
                    if instance.state != 'InService':
                        OutOfService = True
                        break

                if OutOfService:
                    logger.info("Waiting for ELB '%s' instances to be InService (%s sec before timeout)", e['name'], _timeout)
                    _timeout -= 20
                    time.sleep(20)
                    printd()

            if _timeout > 0:
                logger.info("ELB '%s' instances all InService", e['id'])
            else:
                logger.error('ELB instances failed to be InService. ASG processes not resumed.')
                return 1
        for asg in asgs:
            logger.info('Resuming auto scaling processes for %s',asg['id'])
            asg['object'].resume_processes(asg['name'])

    return 0